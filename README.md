# Installation

Vous pouvez cloner et initialiser le projet sur l'environnement de votre choix. Si vous avez des difficultés, vous pouvez utiliser un environnement paiza.cloud comme décrit dans [ce wiki](https://gitlab.com/yvonnigjan/django-exercise/-/wikis/Documentation/Installation-du-projet-sur-paiza.cloud). Pensez à installer les requirements et à appliquer les migrations en exécutant les deux commandes suivantes :

```bash
pip install -r requirements.txt
python3 -m manage migrate
```

Notez que si, par la suite vous souhaitez modifier un fichier `models.py`, il vous faudra ensuite appliquer les commandes suivantes afin de mettre à jour la base de données :

```bash
python3 -m manage makemigrations
python3 -m manage migrate
```

# Documentation

La documentation du modèle est disponible [ici](https://gitlab.com/yvonnigjan/django-exercise/-/wikis/Documentation/Mod%C3%A8le).

Une documentation plus complète de paiza.cloud est disponible [ici](https://engineering.paiza.io/entry/paizacloud_django).

N'hésitez pas à faire des recherches pour les documentations supplémentaires dont vous pourriez avoir besoin (SQL, Django...)

# Test du serveur et utilisation du shell

La commande suivante lance un serveur

```bash
python3 -m manage runserver
```

Le serveur sera ensuite accessible à l'adresse `127.0.0.1:8000`, ou sur paiza.cloud dans la barre de gauche avec l'intitulé "8000".

Le premier serveur lancé ouvre une vue basique définie dans views.py au chemin défini par urls.py.

Il sera aussi possible d'ouvrir une console shell initialisée avec django en appliquant :

```bash
python3 -m manage shell
```

# Exercices

 - Clonez le répertoire, créez-vous une branche, puis créez des commits proposant des solutions à chacune des 3 issues du projet. Pushez ensuite votre branche et ouvrez une merge request (envoyer votre identifiant gitlab à ext.yvonnig.jan@reseau.sncf.fr pour être ajouté en développeur sur le projet et pouvoir push).
 - (Bonus) Ce projet a été fait rapidement et a donc de nombreux défauts. Que pensez-vous qu'il faudrait modifier (dans le code, la donnée, l'architecture, etc.) ? Ouvrez une ou plusieurs issues avec vos idées.
